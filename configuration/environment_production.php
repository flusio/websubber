<?php

return [
    'secret_key' => $dotenv->pop('APP_SECRET_KEY'),

    'url_options' => [
        'protocol' => 'https',
        'host' => $dotenv->pop('APP_HOST'),
        'path' => $dotenv->pop('APP_PATH', '/'),
        'port' => intval($dotenv->pop('APP_PORT', '443')),
    ],

    'database' => [
        'dsn' => "sqlite:{$app_path}/data/db.sqlite",
    ],
];
