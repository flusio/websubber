<?php

$app_path = realpath(__DIR__ . '/..');

include $app_path . '/autoload.php';

\Minz\Configuration::load('dotenv', $app_path);

$request = \Minz\Request::initFromGlobals();

$application = new \App\Application();
$response = $application->run($request);

\Minz\Response::sendByHttp($response);
