.DEFAULT_GOAL := help

USER = $(shell id -u):$(shell id -g)

DOCKER_COMPOSE = docker compose -p websubber -f docker/docker-compose.yml

ifdef NO_DOCKER
	PHP = php
	COMPOSER = composer
	CLI = php cli
else
	PHP = ./docker/bin/php
	COMPOSER = ./docker/bin/composer
	CLI = ./docker/bin/cli
endif

.PHONY: docker-start
docker-start: ## Start a development server
	@echo "Running webserver on http://localhost:8000"
	$(DOCKER_COMPOSE) up

.PHONY: docker-build
docker-build: ## Rebuild the Docker containers
	$(DOCKER_COMPOSE) build

.PHONY: docker-clean
docker-clean: ## Clean the Docker stuff
	$(DOCKER_COMPOSE) down

.PHONY: setup
setup: .env ## Setup the application system
	$(CLI) migrations setup --seed

.PHONY: rollback
rollback: ## Reverse the last migration
ifdef STEPS
	$(CLI) migrations rollback --steps=$(STEPS)
else
	$(CLI) migrations rollback
endif

.PHONY: reset
reset: ## Reset the database
ifndef FORCE
	$(error Please run the operation with FORCE=true)
endif
	$(DOCKER_COMPOSE) stop job_worker
	$(CLI) migrations reset --force --seed
	$(DOCKER_COMPOSE) start job_worker

.PHONY: help
help:
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.env:
	@cp env.sample .env
