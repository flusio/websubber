<?php

namespace App\models;

use Minz\Database;

#[Database\Table(name: 'topics')]
class Topic
{
    use Database\Recordable;

    #[Database\Column]
    public string $id;

    #[Database\Column]
    public \DateTimeImmutable $created_at;

    #[Database\Column]
    public \DateTimeImmutable $updated_at;

    #[Database\Column]
    public ?\DateTimeImmutable $expired_at;

    #[Database\Column]
    public string $url;

    #[Database\Column]
    public string $hub;

    #[Database\Column]
    public string $status;

    #[Database\Column]
    public string $error;

    public function __construct(string $hub_url, string $topic_url)
    {
        $this->id = \Minz\Random::hex(32);
        $this->url = $topic_url;
        $this->hub = $hub_url;
        $this->status = 'new';
        $this->error = '';
    }
}
