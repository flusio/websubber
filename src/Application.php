<?php

namespace App;

use Minz\Request;
use Minz\Response;

/**
 * @phpstan-import-type ResponseReturnable from Response
 */
class Application
{
    /**
     * @return ResponseReturnable
     */
    public function run(Request $request): mixed
    {
        setlocale(LC_ALL, 'fr_FR.UTF8');

        if ($request->method() === 'CLI') {
            $this->initCli($request);
        } else {
            $this->initApp($request);
        }

        return \Minz\Engine::run($request);
    }

    private function initApp(Request $request): void
    {
        $router = Router::load();

        \Minz\Engine::init($router, [
            'controller_namespace' => '\\App\\controllers',
        ]);

        \Minz\Output\View::declareDefaultVariables([
            'environment' => \Minz\Configuration::$environment,
        ]);
    }

    private function initCli(Request $request): void
    {
        $router = Router::loadCli();

        \Minz\Engine::init($router, [
            'controller_namespace' => '\\App\\cli',
        ]);

        \Minz\Output\View::declareDefaultVariables([
            'environment' => \Minz\Configuration::$environment,
        ]);
    }
}
