<?php

namespace App\controllers;

use App\models;
use Minz\Request;
use Minz\Response;

class Topics
{
    public function subscription(Request $request): Response
    {
        $id = $request->param('id', '');

        $mode = $request->param('hub_mode', '');
        $hub_topic = $request->param('hub_topic', '');
        $challenge = $request->param('hub_challenge', '');
        $lease_seconds = $request->paramInteger('hub_lease_seconds', 24 * 60 * 60); // 1 day
        $reason = $request->param('hub_reason', '');

        $topic = models\Topic::findBy(['id' => $id]);

        if (!$topic) {
            return Response::notFound();
        }

        if ($topic->url !== $hub_topic) {
            $this->status = 'failed';
            $this->error = "Hub announces a different topic ({$hub_topic})";
            $this->save();

            return Response::notFound();
        }

        if ($mode === 'denied') {
            $topic->status = 'denied';
            $topic->error = $request->param('@input', '');
            $topic->save();

            return Response::ok();
        } elseif ($mode === 'subscribe') {
            $topic->status = 'subscribed';
            $topic->expired_at = \Minz\Time::fromNow($lease_seconds, 'seconds');
            $topic->error = '';
            $topic->save();

            return Response::text(200, $challenge);
        } else {
            return Response::notFound();
        }
    }
}
