<?php

namespace App\controllers;

use App\models;
use Minz\Request;
use Minz\Response;

class Home
{
    public function show(Request $request): Response
    {
        return Response::ok('home/show.phtml');
    }
}
