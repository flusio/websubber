<?php

namespace App;

class Router extends \Minz\Router
{
    public static function load(): self
    {
        $router = new self();

        $router->addRoute('GET', '/', 'Home#show', 'home');
        $router->addRoute('GET', '/topics/:id/hub', 'Topics#subscription', 'topic subscription');

        return $router;
    }

    public static function loadCli(): self
    {
        $router = self::load();

        $router->addRoute('CLI', '/help', 'Help#show');

        $router->addRoute('CLI', '/jobs', 'Jobs#index');
        $router->addRoute('CLI', '/jobs/watch', 'Jobs#watch');
        $router->addRoute('CLI', '/jobs/run', 'Jobs#run');
        $router->addRoute('CLI', '/jobs/show', 'Jobs#show');
        $router->addRoute('CLI', '/jobs/unfail', 'Jobs#unfail');
        $router->addRoute('CLI', '/jobs/unlock', 'Jobs#unlock');

        $router->addRoute('CLI', '/migrations', 'Migrations#index');
        $router->addRoute('CLI', '/migrations/setup', 'Migrations#setup');
        $router->addRoute('CLI', '/migrations/rollback', 'Migrations#rollback');
        $router->addRoute('CLI', '/migrations/create', 'Migrations#create');
        $router->addRoute('CLI', '/migrations/reset', 'Migrations#reset');

        $router->addRoute('CLI', '/topics', 'Topics#index');
        $router->addRoute('CLI', '/topics/subscribe', 'Topics#subscribe');

        return $router;
    }
}
