<?php

namespace App\cli;

use Minz\Request;
use Minz\Response;

class Help
{
    /**
     * @response 200
     */
    public function show(Request $request): Response
    {
        $usage = "Usage: php cli COMMAND [--OPTION=VALUE]...\n";
        $usage .= "\n";
        $usage .= "COMMAND can be one of the following:\n";
        $usage .= "  help                     Show this help\n";
        $usage .= "\n";
        $usage .= "  topics                   List the subscribed topics\n";
        $usage .= "  topics subscribe         Subscribe to a topic\n";
        $usage .= "      --url=TEXT           The URL of the topic (usually the URL of a Web feed declaring a hub).\n";
        $usage .= "      [--verbose]          Log more information.\n";
        $usage .= "\n";
        $usage .= "  migrations               List the migrations\n";
        $usage .= "  migrations setup         Initialize or migrate the application\n";
        $usage .= "      [--seed=BOOL]        Whether you want to seed the application or not (default: false)\n";
        $usage .= "  migrations rollback      Rollback the latest migrations\n";
        $usage .= "      [--steps=INT]        The number of migrations to rollback\n";
        $usage .= "  migrations create        Create a new migration\n";
        $usage .= "      --name=TEXT          The name of the migration (only chars from A to Z and numbers)\n";
        $usage .= "\n";
        $usage .= "  jobs                     List the registered jobs\n";
        $usage .= "  jobs watch               Wait for and execute jobs\n";
        $usage .= "      [--queue=TEXT]       The name of the queue to wait (default: all)\n";
        $usage .= "      [--stop-after=INT]   The max number of jobs to execute (default is infinite)\n";
        $usage .= "      [--sleep-duration=INT] The number of seconds between two cycles (default: 3)\n";
        $usage .= "  jobs show                Display info about a job\n";
        $usage .= "      --id=ID              The ID of the job\n";
        $usage .= "  jobs run                 Execute a single job\n";
        $usage .= "      --id=ID              The ID of the job\n";
        $usage .= "  jobs unfail              Discard the error of a job\n";
        $usage .= "      --id=ID              The ID of the job\n";
        $usage .= "  jobs unlock              Unlock a job\n";
        $usage .= "      --id=ID              The ID of the job\n";

        return Response::text(200, $usage);
    }
}
