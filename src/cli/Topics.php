<?php

namespace App\cli;

use App\models;
use Minz\Request;
use Minz\Response;

class Topics
{
    public function index(Request $request): Response
    {
        $topics = models\Topic::listAll();

        $result = '';
        foreach ($topics as $topic) {
            $result = $topic->url . "\n";
        }

        return Response::text(200, $result);
    }

    public function subscribe(Request $request): Response|\Generator
    {
        $url = $request->param('url', '');

        $verbose = $request->paramBoolean('verbose');

        if (!$url) {
            yield Response::text(400, 'ERROR: The argument --url is required.');
            return;
        }

        $url = \SpiderBits\Url::sanitize($url);

        if (!\SpiderBits\Url::isValid($url)) {
            yield Response::text(400, 'ERROR: The given URL is invalid.');
            return;
        }

        if (models\Topic::existsBy(['url' => $url])) {
            yield Response::text(400, 'ERROR: Already subscribed to this topic.');
            return;
        }

        yield Response::text(200, "Fetching {$url}...");

        $http = new \SpiderBits\Http();

        try {
            $url_response = $http->get($url);
        } catch (\SpiderBits\HttpError $e) {
            yield Response::text(400, "ERROR: Can’t fetch the URL ({$e->getMessage()}).");

            if ($verbose) {
                yield Response::text(400, (string) $e);
            }

            return;
        }

        $data = $url_response->utf8Data();

        if (!$url_response->success) {
            yield Response::text(400, "ERROR: Fetching the URL failed (code {$e->status}).");

            if ($verbose) {
                yield Response::text(400, $data);
            }

            return;
        }

        yield Response::text(200, "Parsing {$url}...");

        $content_type = $url_response->header('content-type', '');
        if (!\SpiderBits\feeds\Feed::isFeedContentType($content_type)) {
            yield Response::text(400, "ERROR: URL content is not a Web feed (content type {$content_type}).");

            return;
        }

        if (!$data) {
            yield Response::text(400, 'ERROR: URL content is empty.');
            return;
        }

        try {
            $feed = \SpiderBits\feeds\Feed::fromText($data);
        } catch (\Exception $e) {
            yield Response::text(400, 'ERROR: URL content cannot be parsed as a Web feed.');

            if ($verbose) {
                yield Response::text(400, $data);
            }

            return;
        }

        if (!isset($feed->links['hub'])) {
            yield Response::text(400, 'ERROR: URL feed doesn’t declare a hub URL.');

            if ($verbose) {
                yield Response::text(400, $data);
            }

            return;
        }

        $feed_hub_url = \SpiderBits\Url::sanitize($feed->links['hub']);

        if (!\SpiderBits\Url::isValid($feed_hub_url)) {
            yield Response::text(400, "ERROR: The feed hub URL is invalid ({$feed_hub_url}).");
            return;
        }

        if (!isset($feed->links['self'])) {
            yield Response::text(400, 'ERROR: URL feed doesn’t declare a self URL.');

            if ($verbose) {
                yield Response::text(400, $data);
            }

            return;
        }

        $feed_self_url = \SpiderBits\Url::sanitize($feed->links['self']);

        if (!\SpiderBits\Url::isValid($feed_self_url)) {
            yield Response::text(400, "ERROR: The feed self URL is invalid ({$feed_self_url}).");
            return;
        }

        yield Response::text(200, 'Subscribing to the hub...');

        yield Response::text(200, "Use hub {$feed_hub_url}");
        yield Response::text(200, "Use topic {$feed_self_url}");

        $topic = new models\Topic($feed_hub_url, $feed_self_url);
        $topic->save();

        try {
            $callback = \Minz\Url::absoluteFor('topic subscription', ['id' => $topic->id]);
            $hub_response = $http->post($topic->hub, [
                'hub.callback' => $callback,
                'hub.mode' => 'subscribe',
                'hub.topic' => $topic->url,
                'hub.lease_seconds' => 15 * 24 * 60 * 60, // 15 days
                'hub.verify' => 'async',
            ]);
        } catch (\SpiderBits\HttpError $e) {
            $topic->remove();

            yield Response::text(400, "ERROR: Can’t ping the hub ({$e->getMessage()}).");
            return;
        }

        $data = $hub_response->utf8Data();

        if ($hub_response->status !== 202 && $hub_response->status !== 204) {
            $topic->remove();

            yield Response::text(400, "ERROR: Hub didn’t accept subscription (code {$hub_response->status}).");

            if ($verbose) {
                yield Response::text(400, $data);
            }

            return;
        }

        yield Response::text(200, 'Waiting for the hub verification...');

        $tries = 0;
        do {
            if ($tries === 20) { // ~1 minute
                yield Response::text(200, "NOTICE: make sure that {$callback} is reachable from the Internet.");
            }

            if ($tries >= 100) { // ~5 minutes
                break;
            }

            sleep(3);

            $tries += 1;
            $topic = $topic->reload();
        } while ($topic->status === 'new');

        if ($topic->status === 'subscribed') {
            yield Response::text(200, 'Subscription accepted, wonderful!');
        } elseif ($topic->status === 'denied') {
            yield Response::text(200, "Subscription denied (reason: {$topic->error})");
        } elseif ($topic->status === 'failed') {
            yield Response::text(200, "Subscription failed (reason: {$topic->error})");
        } else {
            $topic->remove();

            yield Response::text(400, 'Subscription canceled (hub timeout).');
        }
    }
}
