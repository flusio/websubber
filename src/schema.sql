CREATE TABLE topics (
    id text NOT NULL PRIMARY KEY,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    expired_at datetime,

    url text NOT NULL,
    hub text NOT NULL,
    status text NOT NULL,
    error text NOT NULL
);

CREATE TABLE jobs (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ NOT NULL,
    perform_at TIMESTAMPTZ NOT NULL,
    name TEXT NOT NULL DEFAULT '',
    args JSON NOT NULL DEFAULT '{}',
    frequency TEXT NOT NULL DEFAULT '',
    queue TEXT NOT NULL DEFAULT 'default',
    locked_at TIMESTAMPTZ,
    number_attempts BIGINT NOT NULL DEFAULT 0,
    last_error TEXT NOT NULL DEFAULT '',
    failed_at TIMESTAMPTZ
);
